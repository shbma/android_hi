package com.test.michael.hi;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.os.Handler;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView panoramImage;
    Button changeImageButton;
    TextView techList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        setSupportActionBar(toolbar);

        setImage();
        changePictureToOther();//подменим картинку
        setImageChangeButtonText();
        setImageChangeButtonBehaviour();


        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    //покажем картинку
    private void setImage(){
        panoramImage.setImageResource(R.drawable.panoram);
    }

    //пропишем поведение системы при нажатии нашей кнопки - восстановить картинку
    private void setImageChangeButtonBehaviour(){
        changeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                panoramImage.setImageResource(R.drawable.panoram);
            }
        });
    }

    //установим текст кнопке - возьмем из ресурсов
    private void setImageChangeButtonText(){
        changeImageButton.setText(getText(R.string.action_settings));
        //panoramImage.setImageDrawable(getResources().getDrawable(R.drawable.panoram));
    }

    private void setChangeImageButtonTextColor(){
        changeImageButton.setTextColor(getResources().getColor(R.color.buttn_text_color));
    }

    //выведем массив технлогий
    private void setTechList(){
        String[] tech = this.getResources().getStringArray(R.array.tech); //получим массив

        StringBuilder sBuilder = new StringBuilder();

        for (int i = 0; i < tech.length; i++){
            sBuilder.append(tech[i]+"\n");
        }
        techList.setText(sBuilder.toString());
    }

    //инициализируем view
    private void initViews(){
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        panoramImage = (ImageView) findViewById(R.id.panoram_image);
        changeImageButton = (Button) findViewById(R.id.change_image_butt);
        techList = (TextView) findViewById(R.id.tech_list);
    }

    //заменяем картинку
    private void changePictureToOther(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              panoramImage.setImageResource(R.drawable.sharf);
            }
        },2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
